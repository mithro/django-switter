from datetime import datetime, timedelta

from django.http import HttpResponse, HttpResponseBadRequest
from django.views.generic import View
from django.utils.http import urlunquote
import json

from switter import QUERY_TYPES
from switter.models import CachedTweets
from switter.settings import CACHE_TIME
from switter.utils import urlunquote, now

import urllib, urlparse

def jsonp_wrapper(callback, jsondata):
    # note: jsondata is expected to be a string, already serialised by json.dumps
    if not callback:
        return jsondata
    return ("%s(" % callback) + jsondata + ");"


class SwitterTweetsView(View):
    """
    An AJAX view for fetching tweets. Returns JSON list of tweets.
    """

    def get(self, request, query_type):
        q = dict(urlparse.parse_qsl(urlunquote(self.request.META['QUERY_STRING'])))

        if '_' in q:
	    del q['_']

        callback = None
        if 'callback' in q:
	    callback = q['callback']
	    del q['callback']

        q = urllib.urlencode(q)

        if query_type not in QUERY_TYPES:
            # Bad request, return error message
            valid_choices = ", ".join(QUERY_TYPES)
            content = {"error": "Invalid query type. Valid types are: %s." % valid_choices}
            return HttpResponseBadRequest(
                jsonp_wrapper(callback, json.dumps(content)),
                content_type="application/json"
            )

        cached_tweets, created = CachedTweets.objects.get_or_create(
                                    query_type=query_type,
                                    query_value=q
                                )

        # fetch new tweets from twitter only if cached ones are too old
        min_age = now() - timedelta(seconds=CACHE_TIME)

        if created or cached_tweets.modified < min_age:
            cached_tweets.update_tweets()

        return HttpResponse(
            jsonp_wrapper(callback, cached_tweets.cached_response),
            content_type="application/json"
        )
